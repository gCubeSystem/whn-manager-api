This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Test WhnManager-api

## [v2.0.2-SNAPSHOT] - 2024-01-22

- porting to smartgears 4

